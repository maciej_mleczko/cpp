#ifndef CPP_TRAIN_DYNAMIC_MATRIX_ALLOCATOR_H_
#define CPP_TRAIN_DYNAMIC_MATRIX_ALLOCATOR_H_

#include <memory>

#include "dynamic_matrix_container.h"
#include "i_matrix_allocator.h"
#include "i_matrix_container.h"

namespace matrix {

template <typename T>
class DynamicMatrixAllocator : public IMatrixAllocator<T> {
   public:
    std::unique_ptr<IMatrixContainer<T>> resizeMatrixContainer(
        std::size_t newRows, std::size_t newColumns) const noexcept override {
        return std::make_unique<DynamicMatrixContainer<T>>(newRows, newColumns);
    }
};
}  // namespace matrix

#endif