#include "template_matrix_test.h"

#include <array>
#include <cmath>

TEST_F(TemplateMatrixTest, getTest2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 2, 0};
    TemplateMatrixContainer<decltype(testType), rows, columns> matrix = {
        {3, 2}, {2, 0}};
    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(TemplateMatrixTest, getTest3x2InitListInit) {
    int testType;
    const std::size_t rows = 3, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1,
                                                                2, 0, 6};
    TemplateMatrixContainer<decltype(testType), rows, columns> matrix = {
        {3, 2}, {1, 2}, {0, 6}};
    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(TemplateMatrixTest, getTest2x3InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 3;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1,
                                                                2, 0, 6};
    TemplateMatrixContainer<decltype(testType), rows, columns> matrix = {
        {3, 2, 1}, {2, 0, 6}};
    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(TemplateMatrixTest, setTest2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{1, 5, 7, 9};
    TemplateMatrixContainer<decltype(testType), rows, columns> matrix = {
        {3, 2}, {2, 0}};

    for (unsigned i = 0; i < rows * columns; i++) {
        matrix.set(floor(i / columns), i % columns, expectedData[i]);
    }

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(TemplateMatrixTest, getSize2x2InitListInit) {
    int testType;
    const std::size_t expectedRows = 2, expectedColumns = 2;
    TemplateMatrixContainer<decltype(testType), expectedRows, expectedColumns>
        matrix = {{3, 2}, {2, 0}};
    auto size = matrix.getSize();
    std::size_t rows = size.first;
    std::size_t columns = size.second;
    ASSERT_EQ(rows, expectedRows);
    ASSERT_EQ(columns, expectedColumns);
}

TEST_F(TemplateMatrixTest, copyConstructor2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 2, 0};
    TemplateMatrixContainer<decltype(testType), rows, columns> matrix = {
        {3, 2}, {2, 0}};

    IMatrixContainer<decltype(testType)> &iMatrix = matrix;
    TemplateMatrixContainer<decltype(testType), rows, columns> copiedMatrix(
        iMatrix);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix.get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}

TEST_F(TemplateMatrixTest, copyConstructor2x4InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 4;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1, 0,
                                                                9, 8, 7, 6};
    TemplateMatrixContainer<decltype(testType), rows, columns> matrix = {
        {3, 2, 1, 0}, {9, 8, 7, 6}};

    IMatrixContainer<decltype(testType)> &iMatrix = matrix;
    TemplateMatrixContainer<decltype(testType), rows, columns> copiedMatrix(
        iMatrix);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix.get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}

TEST_F(TemplateMatrixTest, cloneAndResizeSameSize) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1, 0};
    TemplateMatrixContainer<decltype(testType), rows, columns> matrix = {
        {3, 2}, {1, 0}};

    auto copiedMatrix = matrix.cloneAndResizeMatrixContainer(rows, columns);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix->get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}