#!/usr/bin/env python3

import docker
import sys
import shutil
import argparse
import os
import time
import webbrowser

dockerImage='swaydocker/cp-dk:latest'
workDir='/home/developer/cpp'

def runDocker(cmd):
    client = docker.from_env()
    container = client.containers.run(dockerImage, command=cmd, tty=True, remove=True, user='developer', volumes={os.getcwd(): {'bind': workDir, 'mode': 'rw'}}, working_dir=workDir, detach=True)
    log=container.logs(stream=True)
    for i in log:
        print(i.decode("utf-8"), end="")
    sys.exit((container.wait())["StatusCode"])

def build(mode):
    if mode in ("Release", "Debug"):
        runDocker(cmd = 'sh ./docker/build.sh ' + mode)
    else:
        print("Invalid build option")
        sys.exit(1)

def test(mode):
    if mode in ("cov", "no-cov"):
        runDocker(cmd = 'sh ./docker/test.sh ' + mode)
    else:
        print("Invalid build option")
        sys.exit(1)

def clean():
    if os.path.exists("build"):
        shutil.rmtree("build")
    if os.path.exists("test/build"):
        shutil.rmtree("test/build")

def cformat(mode):
    if mode in ("fix", "check"):
        runDocker(cmd = 'sh ./docker/format.sh ' + mode)
    else:
        print("Invalid format option")
        sys.exit(1)

def generateDocs():
    runDocker(cmd = 'doxygen doxygen/doxygen')

def docs():
    webbrowser.open('docs/html/index.html')

#main
parser = argparse.ArgumentParser()
parser.add_argument("--build", "-b", help="Build binary in release or debug mode (Values: Release, Debug)")
parser.add_argument("--test", "-t", help="Build test with or without code coverage (Values: cov, no-cov)")
parser.add_argument("--clean", "-c", help="Remove build folders", action='store_true')
parser.add_argument("--format", "-f", help="Run clang-format (Values: fix, check)")
parser.add_argument("--gendocs", "-g", help="Generate documentation", action='store_true')
parser.add_argument("--docs", "-d", help="View documentation", action='store_true')

args = parser.parse_args()

if args.build:
    build(args.build)
elif args.test:
    test(args.test)
elif args.clean:
    clean()
elif args.format:
    cformat(args.format)
elif args.gendocs:
    generateDocs()
elif args.docs:
    docs()
