#ifndef CPP_TRAIN_MATRIX_H_
#define CPP_TRAIN_MATRIX_H_

#include <cmath>
#include <concepts>
#include <memory>

#include "i_matrix_container.h"

namespace matrix {

/**
 * \brief   .
 */
template <typename T>
class Matrix {
    std::unique_ptr<IMatrixContainer<T>> matrix_;

   public:
    Matrix() = delete;
    Matrix(std::unique_ptr<IMatrixContainer<T>> underlayingContainer)
        : matrix_(std::move(underlayingContainer)) {}
    ~Matrix() = default;
    Matrix(const Matrix<T> &inputMatrix)
        : matrix_(
              inputMatrix.getUnderlayingContainer().cloneMatrixContainer()) {}
    Matrix(const Matrix<T> &inputMatrix, std::size_t rows, std::size_t columns)
        : matrix_(inputMatrix.getUnderlayingContainer()
                      .cloneAndResizeMatrixContainer(rows, columns)) {}
    Matrix(Matrix<T> &&) = default;

    /**
     * \brief   Retrive value of matrix at given postion.
     *
     * \param[in]     row       Matrix row index
     * \param[in]     column    Matrix column index
     *
     * \return        Value at given postion of matrix.
     */
    T get(std::size_t row, std::size_t column) const {
        return matrix_->get(row, column);
    }

    /**
     * \brief   Set value of matrix at given postion.
     *
     * \param[in]     row       Matrix row index
     * \param[in]     column    Matrix column index
     * \param[in]     inputValue    Value to set at given postion
     */
    void set(std::size_t row, std::size_t column, T inputValue) {
        matrix_->set(row, column, inputValue);
    }

    /**
     * \brief   Get number of rows and columns.
     *
     * \return  std::pair of row and column.
     */
    auto getSize() const { return matrix_->getSize(); }

    /**
     * \brief   Get formatted string representation of matrix.
     *
     * \return  String representing matrix separated by ',' and '{' '}' and
     * newlines.
     */
    std::string toString() const { return matrix_->toString(); }

    /**
     * \brief   Get matrix data container with data.
     *
     * \return  Reference to container holding matrix data.
     */
    IMatrixContainer<T> &getUnderlayingContainer() const { return *matrix_; }

    /**
     * \brief   Calculate determinant of matrix.
     *
     * \return  Determinant.
     */
    T determinant() const {
        auto size = matrix_->getSize();
        std::size_t rows = size.first, columns = size.second;
        assert(rows == columns);
        Matrix<T> calculationMatrix(*this);

        T det = 0;
        for (std::size_t rowCount = 0; rowCount < rows; rowCount++) {
            T cof = cofactor(*this, rowCount, columns);
            det += matrix_->get(0, rowCount) * cof;
        }
        return det;
    }

    Matrix<T> operator+(const Matrix &inputMatrix) const {
        Matrix<T> outputMatrix(*this);
        auto matrixAddition = [&](std::size_t row, std::size_t column) -> bool {
            outputMatrix.set(
                row, column,
                outputMatrix.get(row, column) + inputMatrix.get(row, column));
            return true;
        };
        matrixTraverseHelper<decltype(matrixAddition)>(matrixAddition);
        return outputMatrix;
    }

    Matrix<T> operator-(const Matrix &inputMatrix) const {
        Matrix<T> outputMatrix(*this);
        auto matrixSubtraction = [&](std::size_t row,
                                     std::size_t column) -> bool {
            outputMatrix.set(
                row, column,
                outputMatrix.get(row, column) - inputMatrix.get(row, column));
            return true;
        };
        matrixTraverseHelper<decltype(matrixSubtraction)>(matrixSubtraction);
        return outputMatrix;
    }

    Matrix operator*(const Matrix &inputMatrix) const {
        auto lhsSize = matrix_->getSize();
        auto rhsSize = inputMatrix.getSize();
        std::size_t lhsRows = lhsSize.first, lhsColumns = lhsSize.second;
        std::size_t rhsColumns = rhsSize.second;
        Matrix<T> outputMatrix(*this, lhsRows, rhsColumns);

        for (std::size_t rowCount = 0; rowCount < lhsRows; rowCount++) {
            for (std::size_t columnCount = 0; columnCount < rhsColumns;
                 columnCount++) {
                T value = 0;
                for (std::size_t posIt = 0; posIt < lhsColumns; posIt++) {
                    value += matrix_->get(rowCount, posIt) *
                             inputMatrix.get(posIt, columnCount);
                }
                outputMatrix.set(rowCount, columnCount, value);
            }
        }

        return outputMatrix;
    }

    template <typename M>
    Matrix operator*(M multiplier) const {
        Matrix<T> outputMatrix(*this);
        auto matrixValueMultiplication = [&](std::size_t row,
                                             std::size_t column) -> bool {
            outputMatrix.set(row, column,
                             outputMatrix.get(row, column) * multiplier);
            return true;
        };
        matrixTraverseHelper<decltype(matrixValueMultiplication)>(
            matrixValueMultiplication);
        return outputMatrix;
    }

    Matrix operator/(const Matrix &) const = delete;

    bool operator==(const Matrix &inputMatrix) const {
        auto comparator = [&](std::size_t row, std::size_t column) -> bool {
            if (get(row, column) != inputMatrix.get(row, column)) {
                return false;
            } else
                return true;
        };
        return matrixTraverseHelper<decltype(comparator)>(comparator);
    }

    Matrix &operator=(Matrix &&inputMatrix) {
        matrix_ = std::move(inputMatrix.matrix_);
        return *this;
    }

   private:
    template <typename Fun>
    requires std::predicate<Fun, std::size_t, std::size_t> bool
    matrixTraverseHelper(Fun callable) const {
        auto size = matrix_->getSize();
        std::size_t rows = size.first, columns = size.second;
        for (std::size_t rowCount = 0; rowCount < rows; rowCount++) {
            for (std::size_t columnCount = 0; columnCount < columns;
                 columnCount++) {
                if (callable(rowCount, columnCount) != true) {
                    return false;
                }
            }
        }
        return true;
    }

    T cofactor(const Matrix &matrix, std::size_t row, std::size_t size) const {
        T cofactorResult = 0;
        if (size == 1) {
            cofactorResult = matrix.get(0, 0);
        } else {
            std::size_t nextSize = size - 1;
            std::size_t partialRowCount = 0, partialColumnCount = 0;
            Matrix partialMatrix = Matrix(matrix);
            for (std::size_t rowCount = 1; rowCount < size; rowCount++) {
                for (std::size_t columnCount = 0; columnCount < size;
                     columnCount++) {
                    if (columnCount != row) {
                        partialMatrix.set(partialRowCount, partialRowCount,
                                          matrix.get(rowCount, columnCount));
                        partialColumnCount++;
                        if (partialColumnCount > nextSize - 1) {
                            partialRowCount++;
                            partialColumnCount = 0;
                        }
                    }
                }
            }
            for (std::size_t rowCount = 0; rowCount < nextSize; rowCount++) {
                cofactorResult +=
                    pow(-1, row) * cofactor(partialMatrix, rowCount, nextSize);
            }
        }
        return cofactorResult;
    }
};

}  // namespace matrix

#endif