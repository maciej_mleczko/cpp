#include "dynamic_matrix_test.h"

#include <array>
#include <cmath>

TEST_F(DynamicMatrixTest, getTest2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 2, 0};
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2}, {2, 0}};
    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(DynamicMatrixTest, getTest3x2InitListInit) {
    int testType;
    const std::size_t rows = 3, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1,
                                                                2, 0, 6};
    DynamicMatrixContainer<decltype(testType)> matrix = {
        {3, 2}, {1, 2}, {0, 6}};
    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(DynamicMatrixTest, getTest2x3InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 3;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1,
                                                                2, 0, 6};
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2, 1}, {2, 0, 6}};
    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(DynamicMatrixTest, setTest2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{1, 5, 7, 9};
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2}, {2, 0}};

    for (unsigned i = 0; i < rows * columns; i++) {
        matrix.set(floor(i / columns), i % columns, expectedData[i]);
    }

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(DynamicMatrixTest, getSize2x2InitListInit) {
    int testType;
    const std::size_t expectedRows = 2, expectedColumns = 2;
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2}, {2, 0}};
    auto size = matrix.getSize();
    std::size_t rows = size.first;
    std::size_t columns = size.second;
    ASSERT_EQ(rows, expectedRows);
    ASSERT_EQ(columns, expectedColumns);
}

TEST_F(DynamicMatrixTest, copyConstructor2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 2, 0};
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2}, {2, 0}};

    IMatrixContainer<decltype(testType)> &iMatrix = matrix;
    DynamicMatrixContainer<decltype(testType)> copiedMatrix(iMatrix);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix.get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}

TEST_F(DynamicMatrixTest, copyConstructor2x4InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 4;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1, 0,
                                                                9, 8, 7, 6};
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2, 1, 0},
                                                         {9, 8, 7, 6}};

    IMatrixContainer<decltype(testType)> &iMatrix = matrix;
    DynamicMatrixContainer<decltype(testType)> copiedMatrix(iMatrix);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix.get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}

TEST_F(DynamicMatrixTest, clone) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1, 0};
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2}, {1, 0}};

    auto copiedMatrix = matrix.cloneMatrixContainer();

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix->get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}

TEST_F(DynamicMatrixTest, cloneAndResize) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2}, {1, 0}};

    auto copiedMatrix =
        matrix.cloneAndResizeMatrixContainer(rows + 1, columns + 1);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix->get(floor(i / columns), i % columns), 0);
    }
}

TEST_F(DynamicMatrixTest, cloneAndResizeSameSize) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1, 0};
    DynamicMatrixContainer<decltype(testType)> matrix = {{3, 2}, {1, 0}};

    auto copiedMatrix = matrix.cloneAndResizeMatrixContainer(rows, columns);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix->get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}