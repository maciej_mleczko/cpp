#!/bin/sh

# exit when any command fails
set -e

if [ $1 = "fix" ]; then
    find app test/tests -regex '.*\.\(cpp\|hpp\|cu\|c\|h\)' | xargs clang-format-10 --Werror -style="{BasedOnStyle: google, IndentWidth: 4}" -i
    if [ $? = 0 ]; then
        echo "Formatting done"
    fi
else
    find app test/tests -regex '.*\.\(cpp\|hpp\|cu\|c\|h\)' | xargs clang-format-10 --Werror -style="{BasedOnStyle: google, IndentWidth: 4}" -n -i
    if [ $? = 0 ]; then
        echo "Code is formatted correctly"
    fi
fi