cmake_minimum_required(VERSION 3.13)

project("TestMatrix")

file(GLOB SRC_FILES "*.cpp")

add_library(${PROJECT_NAME} OBJECT ${SRC_FILES})
target_include_directories(${PROJECT_NAME} PUBLIC .)
target_link_libraries(${PROJECT_NAME} gtest Matrix)