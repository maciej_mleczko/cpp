#include "static_matrix_2x2_test.h"

#include <array>
#include <cmath>

TEST_F(StaticMatrix2x2Test, getTest2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 2, 0};
    StaticMatrixContainer2x2<decltype(testType)> matrix = {{3, 2}, {2, 0}};
    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(StaticMatrix2x2Test, setTest2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{1, 5, 7, 9};
    StaticMatrixContainer2x2<decltype(testType)> matrix = {{3, 2}, {2, 0}};

    for (unsigned i = 0; i < rows * columns; i++) {
        matrix.set(floor(i / columns), i % columns, expectedData[i]);
    }

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(matrix.get(floor(i / columns), i % columns), expectedData[i]);
    }
}

TEST_F(StaticMatrix2x2Test, getSize2x2InitListInit) {
    int testType;
    const std::size_t expectedRows = 2, expectedColumns = 2;
    StaticMatrixContainer2x2<decltype(testType)> matrix = {{3, 2}, {2, 0}};
    auto size = matrix.getSize();
    std::size_t rows = size.first;
    std::size_t columns = size.second;
    ASSERT_EQ(rows, expectedRows);
    ASSERT_EQ(columns, expectedColumns);
}

TEST_F(StaticMatrix2x2Test, copyConstructor2x2InitListInit) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 2, 0};
    StaticMatrixContainer2x2<decltype(testType)> matrix = {{3, 2}, {2, 0}};

    IMatrixContainer<decltype(testType)> &iMatrix = matrix;
    StaticMatrixContainer2x2<decltype(testType)> copiedMatrix(iMatrix);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix.get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}

TEST_F(StaticMatrix2x2Test, clone) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1, 0};
    StaticMatrixContainer2x2<decltype(testType)> matrix = {{3, 2}, {1, 0}};

    auto copiedMatrix = matrix.cloneMatrixContainer();

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix->get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}

TEST_F(StaticMatrix2x2Test, cloneAndResize) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    StaticMatrixContainer2x2<decltype(testType)> matrix = {{3, 2}, {1, 0}};

    auto copiedMatrix =
        matrix.cloneAndResizeMatrixContainer(rows + 1, columns + 1);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix->get(floor(i / columns), i % columns), 0);
    }
}

TEST_F(StaticMatrix2x2Test, cloneAndResizeSameSize) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::array<decltype(testType), rows * columns> expectedData{3, 2, 1, 0};
    StaticMatrixContainer2x2<decltype(testType)> matrix = {{3, 2}, {1, 0}};

    auto copiedMatrix = matrix.cloneAndResizeMatrixContainer(rows, columns);

    for (unsigned i = 0; i < rows * columns; i++) {
        ASSERT_EQ(copiedMatrix->get(floor(i / columns), i % columns),
                  expectedData[i]);
    }
}
