#!/bin/sh

set -e

mkdir -p test/build

cov=0
buildCmd='valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./test/build/test/tests/cpp-test'

if [ $1 = "cov" ]; then
    cov=1
    buildCmd='make -C test/build cpp-test_cov'
fi

cmake -DCMAKE_BUILD_TYPE=Debug -DTEST=1 -DCOV=$cov -B test/build
make -j$(nproc) -C test/build
eval $buildCmd

