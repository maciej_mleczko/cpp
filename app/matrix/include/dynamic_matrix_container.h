#ifndef CPP_TRAIN_DYNAMIC_MATRIX_CONTAINER_H_
#define CPP_TRAIN_DYNAMIC_MATRIX_CONTAINER_H_

#include <vector>

#include "i_matrix_container.h"

namespace matrix {

template <typename T>
class DynamicMatrixContainer : public IMatrixContainer<T> {
    std::vector<std::vector<T>> matrix_;

   public:
    DynamicMatrixContainer() = delete;
    DynamicMatrixContainer(std::size_t row, std::size_t column)
        : matrix_(row, std::vector<T>(column)) {}
    DynamicMatrixContainer(
        std::initializer_list<std::initializer_list<T>> initializer)
        : matrix_(std::vector<std::vector<T>>(initializer.begin(),
                                              initializer.end())) {}
    ~DynamicMatrixContainer() = default;
    DynamicMatrixContainer(const DynamicMatrixContainer &) = default;
    DynamicMatrixContainer(const IMatrixContainer<T> &inputMatrix) {
        auto size = inputMatrix.getSize();
        std::size_t rows = size.first;
        std::size_t columns = size.second;
        matrix_.reserve(rows);
        for (std::size_t rowCount = 0; rowCount < rows; rowCount++) {
            matrix_.emplace_back(std::vector<T>(columns));
            for (std::size_t columnCount = 0; columnCount < columns;
                 columnCount++) {
                matrix_[rowCount][columnCount] =
                    inputMatrix.get(rowCount, columnCount);
            }
        }
    }
    DynamicMatrixContainer(DynamicMatrixContainer &&) = default;
    DynamicMatrixContainer(IMatrixContainer<T> &&) = delete;
    // container operations
    T get(std::size_t row, std::size_t column) const override {
        return matrix_[row][column];
    }

    void set(std::size_t row, std::size_t column, T inputValue) override {
        matrix_[row][column] = inputValue;
    }

    std::pair<std::size_t, std::size_t> getSize() const override {
        return std::make_pair<std::size_t, std::size_t>(matrix_.size(),
                                                        matrix_[0].size());
    }

    std::unique_ptr<IMatrixContainer<T>> cloneMatrixContainer() const override {
        return std::make_unique<DynamicMatrixContainer<T>>(*this);
    }

    std::unique_ptr<IMatrixContainer<T>> cloneAndResizeMatrixContainer(
        std::size_t newRows, std::size_t newColumns) const override {
        auto size = getSize();
        std::size_t rows = size.first;
        std::size_t columns = size.second;
        if ((newRows == rows) and (newColumns == columns)) {
            return std::make_unique<DynamicMatrixContainer<T>>(*this);
        } else
            return std::make_unique<DynamicMatrixContainer<T>>(newRows,
                                                               newColumns);
    }

    DynamicMatrixContainer &operator=(DynamicMatrixContainer &&inputContainer) {
        matrix_ = std::move(inputContainer.matrix_);
        return *this;
    }
};

}  // namespace matrix

#endif