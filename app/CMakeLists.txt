cmake_minimum_required(VERSION 3.13)

add_subdirectory(matrix)
add_subdirectory(complex_numbers)
