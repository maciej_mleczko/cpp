#ifndef CPP_TRAIN_INTERFACE_MATRIX_ALLOCATOR_H_
#define CPP_TRAIN_INTERFACE_MATRIX_ALLOCATOR_H_

#include "i_matrix_container.h"

namespace matrix {

template <typename T>
struct IMatrixAllocator {
    virtual ~IMatrixAllocator(){};
    /**
     * \brief   Runtime allocation new matrix container
     *
     * \param[in]     newRows       Row amount to allocate
     * \param[in]     newColumns    Coulmn amount to allocate
     *
     * \return  Pointer to newly allocated matrix container.
     */
    virtual std::unique_ptr<IMatrixContainer<T>> resizeMatrixContainer(
        std::size_t newRows, std::size_t newColumns) const = 0;
};

}  // namespace matrix

#endif