cmake_minimum_required(VERSION 3.13)

project("cpp-train")

add_subdirectory(app)

set(CMAKE_CXX_FLAGS "-std=gnu++20 -Werror -Wall -Wpedantic -Wextra -Wshadow -Wnon-virtual-dtor -Wold-style-cast -Wcast-align -Wunused -Woverloaded-virtual -Wsign-conversion -Wformat=2 -Wduplicated-cond -Wduplicated-branches -Wlogical-op")

if(${TEST})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0")
    if(${COV})
        include(cmake/CodeCoverage.cmake)
        append_coverage_compiler_flags()
    endif()
    add_subdirectory(test)
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -Wnull-dereference -Wuseless-cast")
    add_executable(${PROJECT_NAME} app/main.cpp)
    target_link_libraries(${PROJECT_NAME} PUBLIC Matrix ComplexNumbers)
endif()