#include "matrix_test.h"

#include <array>
#include <cmath>
#include <iostream>

TEST_F(MatrixTest, copyTestEquals) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::initializer_list<std::initializer_list<typeof(testType)>> data = {
        {3, 2}, {2, 0}};
    Matrix<typeof(testType)> matrix(
        std::make_unique<
            TemplateMatrixContainer<typeof(testType), rows, columns>>(data));

    Matrix<typeof(testType)> newMatrix(matrix);

    EXPECT_EQ(matrix, newMatrix);
}

TEST_F(MatrixTest, copyTestInequality) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::initializer_list<std::initializer_list<typeof(testType)>> data = {
        {3, 2}, {2, 0}};
    Matrix<typeof(testType)> matrix(
        std::make_unique<
            TemplateMatrixContainer<typeof(testType), rows, columns>>(data));

    Matrix<typeof(testType)> newMatrix(matrix);
    newMatrix.set(0, 0, 255);

    EXPECT_NE(matrix, newMatrix);
}

TEST_F(MatrixTest, additionTest) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataFirstMatrix = {{1, 2}, {3, 4}};
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataSecondMatrix = {{9, 8}, {7, 6}};
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataExpectedMatrix = {{10, 10}, {10, 10}};
    Matrix<typeof(testType)> firstMatrix(
        std::make_unique<
            TemplateMatrixContainer<typeof(testType), rows, columns>>(
            dataFirstMatrix));
    Matrix<typeof(testType)> secondMatrix(
        std::make_unique<
            TemplateMatrixContainer<typeof(testType), rows, columns>>(
            dataSecondMatrix));
    Matrix<typeof(testType)> expectedMatrix(
        std::make_unique<
            TemplateMatrixContainer<typeof(testType), rows, columns>>(
            dataExpectedMatrix));

    Matrix<typeof(testType)> resultMatrix = firstMatrix + secondMatrix;
    EXPECT_EQ(resultMatrix, expectedMatrix);
}

TEST_F(MatrixTest, subtractionTest) {
    int testType;
    const std::size_t rows = 2, columns = 2;
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataFirstMatrix = {{10, 10}, {10, 10}};
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataSecondMatrix = {{9, 8}, {7, 6}};
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataExpectedMatrix = {{1, 2}, {3, 4}};
    Matrix<typeof(testType)> firstMatrix(
        std::make_unique<
            TemplateMatrixContainer<typeof(testType), rows, columns>>(
            dataFirstMatrix));
    Matrix<typeof(testType)> secondMatrix(
        std::make_unique<
            TemplateMatrixContainer<typeof(testType), rows, columns>>(
            dataSecondMatrix));
    Matrix<typeof(testType)> expectedMatrix(
        std::make_unique<
            TemplateMatrixContainer<typeof(testType), rows, columns>>(
            dataExpectedMatrix));

    Matrix<typeof(testType)> resultMatrix = firstMatrix - secondMatrix;
    EXPECT_EQ(resultMatrix, expectedMatrix);
}

TEST_F(MatrixTest, multiplicationTest) {
    int testType;
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataFirstMatrix = {{1, 2, 3}, {4, 5, 6}};
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataSecondMatrix = {{7, 8}, {9, 10}, {11, 12}};
    std::initializer_list<std::initializer_list<typeof(testType)>>
        dataExpectedMatrix = {{58, 64}, {139, 154}};
    Matrix<typeof(testType)> firstMatrix(
        std::make_unique<TemplateMatrixContainer<typeof(testType), 2, 3>>(
            dataFirstMatrix));
    Matrix<typeof(testType)> secondMatrix(
        std::make_unique<TemplateMatrixContainer<typeof(testType), 3, 2>>(
            dataSecondMatrix));
    Matrix<typeof(testType)> expectedMatrix(
        std::make_unique<TemplateMatrixContainer<typeof(testType), 2, 2>>(
            dataExpectedMatrix));

    Matrix<typeof(testType)> resultMatrix = firstMatrix * secondMatrix;
    EXPECT_EQ(resultMatrix, expectedMatrix);
}

TEST_F(MatrixTest, determinantTest2x2) {
    int testType;
    int expectedResult = -2;
    std::initializer_list<std::initializer_list<typeof(testType)>> data = {
        {1, 2}, {3, 4}};
    Matrix<typeof(testType)> matrix(
        std::make_unique<TemplateMatrixContainer<typeof(testType), 2, 2>>(
            data));

    EXPECT_EQ(matrix.determinant(), expectedResult);
}

TEST_F(MatrixTest, determinantTest1x1) {
    int testType;
    int expectedResult = 1;
    std::initializer_list<std::initializer_list<typeof(testType)>> data = {{1}};
    Matrix<typeof(testType)> matrix(
        std::make_unique<TemplateMatrixContainer<typeof(testType), 1, 1>>(
            data));

    EXPECT_EQ(matrix.determinant(), expectedResult);
}

TEST_F(MatrixTest, determinantTest5x5) {
    int testType;
    int expectedResult = 0;
    std::initializer_list<std::initializer_list<typeof(testType)>> data = {
        {1, 2, 3, 4, 5},
        {6, 7, 8, 9, 10},
        {11, 12, 13, 14, 15},
        {16, 17, 18, 19, 20},
        {21, 22, 23, 24, 25}};
    Matrix<typeof(testType)> matrix(
        std::make_unique<TemplateMatrixContainer<typeof(testType), 5, 5>>(
            data));

    EXPECT_EQ(matrix.determinant(), expectedResult);
}