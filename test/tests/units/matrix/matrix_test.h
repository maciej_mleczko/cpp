#ifndef CPP_TRAIN_MATRIX_TEST_H_
#define CPP_TRAIN_MATRIX_TEST_H_

#include <gtest/gtest.h>

#include "dynamic_matrix_container.h"
#include "i_matrix_container.h"
#include "matrix.h"
#include "template_matrix_container.h"

using namespace matrix;

class MatrixTest : public testing::Test {
   protected:
    virtual void SetUp() {}
};

#endif