cmake_minimum_required(VERSION 3.13)

project(cpp-test)

add_subdirectory(mocks)
add_subdirectory(units)

add_executable(${PROJECT_NAME} test_main.cpp)

target_link_libraries(${PROJECT_NAME} gtest gmock 
                        TestMatrix
                        Matrix
                        )

if(${COV})
setup_target_for_coverage_lcov(
    NAME ${PROJECT_NAME}_cov                    # New target name
    EXECUTABLE ${PROJECT_NAME} -j ${PROCESSOR_COUNT} # Executable in PROJECT_BINARY_DIR
    DEPENDENCIES ${PROJECT_NAME}                     # Dependencies to build first
    BASE_DIRECTORY ""                        # Base directory for report
                                                #  (defaults to PROJECT_SOURCE_DIR)
    EXCLUDE "*/googletest-src/*" "*/10/*" "*/units/*"                              # Patterns to exclude (can be relative
                                                #  to BASE_DIRECTORY, with CMake 3.4+)
    NO_DEMANGLE                                 # Don't demangle C++ symbols
                                                #  even if c++filt is found
)
endif()