#ifndef CPP_TRAIN_STATIC_MATRIX_CONTAINER_2x2_H_
#define CPP_TRAIN_STATIC_MATRIX_CONTAINER_2x2_H_

#include "dynamic_matrix_allocator.h"
#include "i_matrix_allocator.h"
#include "i_matrix_container.h"

namespace matrix {

template <typename T, typename Reallocator = DynamicMatrixAllocator<T>>
requires std::derived_from<Reallocator,
                           IMatrixAllocator<T>> class StaticMatrixContainer2x2
    : public IMatrixContainer<T> {
    const static std::size_t rows = 2, columns = 2;
    std::array<std::array<T, rows>, columns> matrix_;
    Reallocator reallocator_;

   public:
    StaticMatrixContainer2x2() = delete;
    StaticMatrixContainer2x2(
        std::initializer_list<std::initializer_list<T>> initializer) {
        std::size_t rowCount = 0;
        for (auto rowIter = initializer.begin(); rowIter != initializer.end();
             rowIter++, rowCount++) {
            std::size_t columnCount = 0;
            for (auto columnIter = rowIter->begin();
                 columnIter != rowIter->end(); columnIter++, columnCount++) {
                matrix_[rowCount][columnCount] = *columnIter;
            }
        }
    }
    ~StaticMatrixContainer2x2() = default;
    StaticMatrixContainer2x2(const StaticMatrixContainer2x2 &) = default;
    StaticMatrixContainer2x2(const IMatrixContainer<T> &inputMatrix) {
        for (std::size_t rowCount = 0; rowCount < rows; rowCount++) {
            for (std::size_t columnCount = 0; columnCount < columns;
                 columnCount++) {
                matrix_[rowCount][columnCount] =
                    inputMatrix.get(rowCount, columnCount);
            }
        }
    }
    StaticMatrixContainer2x2(StaticMatrixContainer2x2 &&) = default;

    T get(std::size_t row, std::size_t column) const override {
        return matrix_[row][column];
    }

    void set(std::size_t row, std::size_t column, T inputValue) override {
        matrix_[row][column] = inputValue;
    }

    std::pair<std::size_t, std::size_t> getSize() const override {
        return std::make_pair<std::size_t, std::size_t>(std::size_t(rows),
                                                        std::size_t(columns));
    }

    std::unique_ptr<IMatrixContainer<T>> cloneMatrixContainer() const override {
        return std::make_unique<StaticMatrixContainer2x2<T>>(*this);
    }

    std::unique_ptr<IMatrixContainer<T>> cloneAndResizeMatrixContainer(
        std::size_t newRows, std::size_t newColumns) const override {
        if ((newRows == rows) and (newColumns == columns)) {
            return std::make_unique<StaticMatrixContainer2x2<T>>(*this);
        } else {
            return reallocator_.resizeMatrixContainer(newRows, newColumns);
        }
    }

    StaticMatrixContainer2x2 &operator=(
        StaticMatrixContainer2x2 &&inputContainer) {
        matrix_ = std::move(inputContainer.matrix_);
        return *this;
    }
};

}  // namespace matrix
#endif