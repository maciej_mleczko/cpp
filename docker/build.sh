#!/bin/sh

# exit when any command fails
set -e

mkdir -p build

cmake -D CMAKE_BUILD_TYPE=Release -S ./ -B build
make -j$(nproc) -C build