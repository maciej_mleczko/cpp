cmake_minimum_required(VERSION 3.13)

project("Matrix")

file(GLOB SRC_FILES "src/*.cpp")

add_library(${PROJECT_NAME} OBJECT ${SRC_FILES})
target_include_directories(${PROJECT_NAME} PUBLIC include interface)
target_link_libraries(${PROJECT_NAME} PUBLIC ComplexNumbers)