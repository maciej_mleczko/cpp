#ifndef CPP_TRAIN_TEMPLATE_MATRIX_CONTAINER_H_
#define CPP_TRAIN_TEMPLATE_MATRIX_CONTAINER_H_

#include <array>
#include <concepts>
#include <memory>

#include "dynamic_matrix_allocator.h"
#include "i_matrix_allocator.h"
#include "i_matrix_container.h"

namespace matrix {

template <typename T, std::size_t rows, std::size_t columns,
          typename Reallocator = DynamicMatrixAllocator<T>>
requires std::derived_from<Reallocator,
                           IMatrixAllocator<T>> class TemplateMatrixContainer
    : public IMatrixContainer<T> {
    std::array<std::array<T, columns>, rows> matrix_;
    Reallocator reallocator_;

   public:
    TemplateMatrixContainer() = delete;
    TemplateMatrixContainer(
        std::initializer_list<std::initializer_list<T>> initializer) {
        std::size_t rowCount = 0;
        for (auto rowIter = initializer.begin(); rowIter != initializer.end();
             rowIter++, rowCount++) {
            std::size_t columnCount = 0;
            for (auto columnIter = rowIter->begin();
                 columnIter != rowIter->end(); columnIter++, columnCount++) {
                matrix_[rowCount][columnCount] = *columnIter;
            }
        }
    }
    ~TemplateMatrixContainer() = default;
    TemplateMatrixContainer(const TemplateMatrixContainer &) = default;
    TemplateMatrixContainer(const IMatrixContainer<T> &inputMatrix) {
        for (std::size_t rowCount = 0; rowCount < rows; rowCount++) {
            for (std::size_t columnCount = 0; columnCount < columns;
                 columnCount++) {
                matrix_[rowCount][columnCount] =
                    inputMatrix.get(rowCount, columnCount);
            }
        }
    }
    TemplateMatrixContainer(TemplateMatrixContainer &&) = default;

    T get(std::size_t row, std::size_t column) const override {
        return matrix_[row][column];
    }

    void set(std::size_t row, std::size_t column, T inputValue) override {
        matrix_[row][column] = inputValue;
    }

    std::pair<std::size_t, std::size_t> getSize() const override {
        return std::make_pair<std::size_t, std::size_t>(rows, columns);
    }

    std::unique_ptr<IMatrixContainer<T>> cloneMatrixContainer() const override {
        return std::make_unique<TemplateMatrixContainer<T, rows, columns>>(
            *this);
    }

    std::unique_ptr<IMatrixContainer<T>> cloneAndResizeMatrixContainer(
        std::size_t newRows, std::size_t newColumns) const override {
        if ((newRows == rows) and (newColumns == columns)) {
            return std::make_unique<TemplateMatrixContainer<T, rows, columns>>(
                *this);
        } else {
            return reallocator_.resizeMatrixContainer(newRows, newColumns);
        }
    }

    TemplateMatrixContainer &operator=(
        TemplateMatrixContainer &&inputContainer) {
        matrix_ = std::move(inputContainer.matrix_);
        return *this;
    }
};

}  // namespace matrix

#endif