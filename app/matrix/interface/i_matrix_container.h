#ifndef CPP_TRAIN_INTERFACE_MATRIX_CONTAINER_H_
#define CPP_TRAIN_INTERFACE_MATRIX_CONTAINER_H_

#include <concepts>
#include <memory>
#include <string>
#include <utility>

#include "complex_number.h"

using namespace complex_numbers;

namespace matrix {

template <typename T>
concept real_number = std::floating_point<T> or std::integral<T>;

template <typename T>
concept imaginary_number =
    std::derived_from<T, ComplexNumber> or std::same_as<T, ComplexNumber>;

template <typename T>
    requires real_number<T> or imaginary_number<T> struct IMatrixContainer {
    virtual ~IMatrixContainer(){};

    /**
     * \brief   Retrive value of matrix at given postion.
     *
     * \param[in]     row       Matrix row index
     * \param[in]     column    Matrix column index
     *
     * \return        Value at given postion of matrix.
     */
    virtual T get(std::size_t row, std::size_t column) const = 0;

    /**
     * \brief   Set value of matrix at given postion.
     *
     * \param[in]     row       Matrix row index
     * \param[in]     column    Matrix column index
     * \param[in]     inputValue    Value to set at given postion
     */
    virtual void set(std::size_t row, std::size_t column, T inputValue) = 0;

    /**
     * \brief   Get number of rows and columns.
     *
     * \return  std::pair of row and column.
     */
    virtual std::pair<std::size_t, std::size_t> getSize() const = 0;

    /**
     * \brief   Get formatted string representation of matrix.
     *
     * \return  String representing matrix separated by ',' and '{' '}' and
     * newlines.
     */
    std::string toString() const {
        auto size = getSize();
        std::size_t rows = size.first;
        std::size_t columns = size.second;
        std::string output = "";
        for (std::size_t rowCount = 0; rowCount < rows; rowCount++) {
            output += "{";
            for (std::size_t columnCount = 0; columnCount < columns;
                 columnCount++) {
                output += std::to_string(get(rowCount, columnCount));
                if (columnCount != columns - 1) {
                    output += ", ";
                }
            }
            output += "}\n";
        }
        return output;
    }

    /**
     * \brief   Perform deep copy on the object.
     *
     * \return  Pointer to cloned object.
     */
    virtual std::unique_ptr<IMatrixContainer<T>> cloneMatrixContainer()
        const = 0;

    /**
     * \brief   Perform deep copy on the object with resize of container
     *
     * \param[in]     newRows       Row amount to allocate
     * \param[in]     newColumns    Coulmn amount to allocate
     *
     * \return  Pointer to cloned object.
     */
    virtual std::unique_ptr<IMatrixContainer<T>> cloneAndResizeMatrixContainer(
        std::size_t newRows, std::size_t newColumns) const = 0;
};

}  // namespace matrix

#endif